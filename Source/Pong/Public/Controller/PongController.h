// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GameMode/PongGameModeBase.h"
#include "PongController.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API APongController : public APlayerController
{
	GENERATED_BODY()
	
public:
	APongController();
	void BeginPlay() override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void OnPawnPossessed();
	AActor* GetPaddleCamera();

	void AssignSide(EPlayerSide Side, AActor* Camera);
	void SetMessage(const FString& Message, float ExpireIn = 5.f);
	void SetMatchTime(int32 MatchTime);
	void SetMatchScore(EPlayerSide Side, int32 Score);
	void MatchEnd();

	UFUNCTION(Client, Reliable)
	void ClientMatchEnd();
	UFUNCTION(Client, Reliable)
	void ClientSetMessage(const FString& Message, float ExpireIn = 5.f);
	UFUNCTION(Client, Reliable)
	void ClientSetMatchTime(int32 MatchTime);
	UFUNCTION(Client, Reliable) // TMaps replication via RPC are not allowed... /shrug
	void ClientSetMatchScore(EPlayerSide Side, int32 Score);
	UFUNCTION(Server, Reliable, WithValidation)
	void Ready();

	UFUNCTION(BlueprintCallable)
	FORCEINLINE EPlayerSide GetPlayerSide() const { return PlayerSide; }
public:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UPongInterface> InterfaceClass;
protected:
	class UPongInterface* Interface;
	TMap<EPlayerSide, int32> Scores;
	UPROPERTY(replicated)
	EPlayerSide PlayerSide;
	UPROPERTY(replicated)
	AActor* PaddleCamera = nullptr;
};
