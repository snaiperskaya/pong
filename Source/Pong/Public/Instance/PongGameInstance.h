// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "PongGameInstance.generated.h"

USTRUCT(BlueprintType)
struct PONG_API FGameSettings
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	float PaddleSpeed = 33.f;
	UPROPERTY(BlueprintReadWrite)
	float BallSpeed = 20.f;
	UPROPERTY(BlueprintReadWrite)
	int32 MinimumPlayers = 2;
	UPROPERTY(BlueprintReadWrite)
	int32 MaxmimumPlayers = 2;
	UPROPERTY(BlueprintReadWrite)
	int32 MatchTime = 120;
};

/**
 * 
 */
UCLASS()
class PONG_API UPongGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite)
	FGameSettings Settings;
	
	bool bIsSinglePlayer = false;
};
