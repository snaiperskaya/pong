// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actor/Ball.h"
#include <CoreMinimal.h>
#include <GameFramework/GameModeBase.h>
#include "PongGameModeBase.generated.h"

UENUM(BlueprintType)
enum class EPlayerSide : uint8
{
	Side1,
	Side2
};

/**
 * 
 */
UCLASS()
class PONG_API APongGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	APongGameModeBase();
	void SetupPlayer(AController* Controller);
	void RestartPlayer(AController* NewPlayer) override;
	void AddReadyPlayer(class APongController* Player);
	void ScorePoint(class APongController* Player);
	AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	FString InitNewPlayer(APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal) override;

protected:
	void BeginPlay() override;
	void Setup();
	void StartGame();
	EPlayerSide GetLowestPlayersSide();
	int32* GetBotSideScore();

	void SendMessage(const FString& Message, float ExpireIn = 5.f);
	void SpawnBall();
	void OnMatchEnd();
public:
	UPROPERTY(EditDefaultsOnly)
	int32 MinPlayers = 0;
	UPROPERTY(EditDefaultsOnly)
	int32 MaxPlayers = 0;
	UPROPERTY(EditAnywhere, NoClear, BlueprintReadOnly, Category = "Classes")
	TSubclassOf<ABall> BallClass;
	UPROPERTY(EditAnywhere, NoClear, BlueprintReadOnly, Category = "Classes")
	int32 MatchTime = 0;
protected:
	FTimerHandle StartGameCountDownTimer;
	FTimerHandle MatchCountDownTimer;
	TMap<EPlayerSide, TArray<class APongController*>> Players;
	TMap<EPlayerSide, int32> Score;
	TArray<class APongController*> ReadyPlayers;
	TArray<AActor*> Cameras;
	bool bHasBeenSetup = false;
	bool bIsStarted = false;
	class ABall* Ball = nullptr;
	class UPongGameInstance* Instance = nullptr;
};
