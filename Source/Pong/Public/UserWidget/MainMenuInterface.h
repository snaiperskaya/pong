// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenuInterface.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API UMainMenuInterface : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UFUNCTION(BlueprintCallable)
	void StartSinglePlayer();

	UFUNCTION(BlueprintCallable)
	void ConnectTo(FString Url);

	// GetWorld()->Listen(url);
	UFUNCTION(BlueprintCallable)
	void StartListening();
	
	UFUNCTION(BlueprintCallable)
	void Quit();

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString SinglePlayerLevel;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString MultiPlayerLevel; 
};
