// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameMode/PongGameModeBase.h"
#include "PongInterface.generated.h"

UENUM(BlueprintType)
enum EMatchResult 
{
	Won,
	Lost,
	Draw
};
/**
 * 
 */
UCLASS()
class PONG_API UPongInterface : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetMessage(const FString& Message, float ExpireIn = 5.f);

	UFUNCTION(BlueprintImplementableEvent)
	void SetMatchTime(int32 MatchTime);

	UFUNCTION(BlueprintImplementableEvent)
	void SetMatchScore(EPlayerSide Side, int32 Score);

	UFUNCTION(BlueprintImplementableEvent)
	void MatchEnd(EMatchResult Result);
};
