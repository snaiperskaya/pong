// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PongArena.generated.h"

/*
We are not hardcoding any mesh here because we want the user to be able to compose 
arenas as they prefer, no matter if using one or more meshes.
*/
UCLASS()
class PONG_API APongArena : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APongArena();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	class UArenaBorderComponent* Border1 = nullptr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	class UArenaBorderComponent* Border2 = nullptr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	class USceneComponent* Root = nullptr;
};
