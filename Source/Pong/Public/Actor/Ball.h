// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ball.generated.h"

UCLASS()
class PONG_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABall();
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void Respawn();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void MoveForward(float DeltaTime);

	UFUNCTION(NetMulticast, Reliable)
	void PlaySound(class USoundBase* Sound);
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Replicated)
	float Speed;
protected:
	class APaddle* LastHitPaddle = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class USoundBase* PaddleHitSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class USoundBase* WallHitSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class USoundBase* ScorePointSound = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USphereComponent* CollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* BallMesh = nullptr;
};
