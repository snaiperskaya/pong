// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Paddle.generated.h"

UCLASS()
class PONG_API APaddle : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APaddle();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	float GetOnHitDirectionModifier(FVector HitWorldLocation);
	bool IsBorder(FVector HitWorldLocation);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void Restart() override;
	void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	void MoveRight(float Value);
	bool CanMoveInDirection(float Value);
	FVector GetSize();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerMoveRight(float Value);
public:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Replicated)
	float Speed;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USceneComponent* RootComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* PaddleMesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UMaterialInterface* PlayerPaddleMaterial;
	UPROPERTY(Replicated)
	FVector Size;
	
};
