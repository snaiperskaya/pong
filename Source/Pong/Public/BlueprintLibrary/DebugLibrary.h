// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DebugLibrary.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API UDebugLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static void DrawDebugSphere(UObject* WorldContextObject, FVector Where, float Radius = 32.f, float Duration = 1.f, FLinearColor Color = FLinearColor(255, 0, 0), int32 Segments = 64, float Thickness = 1.f);
	
	UFUNCTION(BlueprintCallable)
	static void DrawVisibleLineTrace(UObject* WorldContextObject, FVector From, FVector To);
};
