// Fill out your copyright notice in the Description page of Project Settings.

#include "PongGameModeBase.h"
#include "Actor/Paddle.h"
#include "Controller/PongController.h"
#include "Camera/SideCamera.h"
#include "Instance/PongGameInstance.h"
#include <Engine/World.h>
#include <Kismet/GameplayStatics.h>
#include <GameFramework/PlayerStart.h>
#include <GameFramework/PlayerState.h>
#include <Public/TimerManager.h>
#include <Actor/PongArena.h>

APongGameModeBase::APongGameModeBase()
{
	Players.Add(EPlayerSide::Side1);
	Players.Add(EPlayerSide::Side2);

	Score.Add(EPlayerSide::Side1);
	Score.Add(EPlayerSide::Side2);
}

void APongGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	Setup();
}

void APongGameModeBase::Setup()
{
	if (bHasBeenSetup) return;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASideCamera::StaticClass(), Cameras);
	if (Cameras.Num() != 2) {
		UE_LOG(LogTemp, Warning, TEXT("You should have 2 ASideCamera in the current level!"));
		return;
	}

	Instance = Cast<UPongGameInstance>(GetWorld()->GetGameInstance());
	if (Instance) {
		MinPlayers = MinPlayers != 0 ? MinPlayers : Instance->Settings.MinimumPlayers;
		MaxPlayers = MaxPlayers != 0 ? MaxPlayers : Instance->Settings.MaxmimumPlayers;
		MatchTime = MatchTime != 0 ? MatchTime : Instance->Settings.MatchTime;

		if (Instance->bIsSinglePlayer) {
			MinPlayers = 1;
			MaxPlayers = 1;
		}
	}

	bHasBeenSetup = true;
}

FString APongGameModeBase::InitNewPlayer(APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal)
{
	SetupPlayer(NewPlayerController);
	return Super::InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);
}

void APongGameModeBase::SetupPlayer(AController* Controller)
{
	Setup();

	auto PongController = Cast<APongController>(Controller);
	if (!PongController) {
		UE_LOG(LogTemp, Warning, TEXT("Game mode default controller is not set to a subclass of APongController! Cannot spawn."));
		return;
	}
	
	// Already initialised
	if (PongController->GetPaddleCamera()) {
		return;
	}

	// If we are in a single player match we always spawn on the opposite side of the AI
	if (Instance && Instance->bIsSinglePlayer) {
		TArray<AActor*> PlayerStarts;
		UGameplayStatics::GetAllActorsOfClass(this, APlayerStart::StaticClass(), PlayerStarts);
		for (auto Actor : PlayerStarts) {
			auto PlayerStart = Cast<APlayerStart>(Actor);
			if (PlayerStart && PlayerStart->PlayerStartTag == TEXT("SinglePlayer")) {
				TArray<AActor*> Children;
				PlayerStart->GetAttachedActors(Children);
				if (Children.Num() > 0 && Cameras.Contains(Children[0])) {
					auto Index = Cameras.Find(Children[0]);
					Players[(EPlayerSide)Index].AddUnique(PongController);
					PongController->AssignSide((EPlayerSide)Index, Cameras[Index]);
				}
			}
		}
		Instance->bIsSinglePlayer = false;
	} else {
		auto Side = GetLowestPlayersSide();
		Players[Side].AddUnique(PongController);
		PongController->AssignSide(Side, Cameras[(uint8)Side]);
	}
}

void APongGameModeBase::RestartPlayer(AController* NewPlayer)
{
	SetupPlayer(NewPlayer);
	Super::RestartPlayer(NewPlayer);
}

AActor* APongGameModeBase::ChoosePlayerStart_Implementation(AController* Player)
{
	auto PlayerController = Cast<APongController>(Player);
	if (PlayerController) {
		if (!PlayerController->GetPaddleCamera()) {
			UE_LOG(LogTemp, Warning, TEXT("No camera assigned to the controller."));
			return nullptr;
		}

		auto PlayerStart = PlayerController->GetPaddleCamera()->GetAttachParentActor();
		if (!PlayerStart) {
			UE_LOG(LogTemp, Warning, TEXT("Camera %s has is not parented to a Player Start!"), *PlayerController->GetPaddleCamera()->GetName());
			return nullptr;
		}

		return PlayerStart;
	}

	return nullptr;
}

EPlayerSide APongGameModeBase::GetLowestPlayersSide()
{
	if (Players[EPlayerSide::Side1].Num() <= Players[EPlayerSide::Side2].Num()) {
		return EPlayerSide::Side1;
	}

	return EPlayerSide::Side2;
}

void APongGameModeBase::SendMessage(const FString& Message, float ExpireIn)
{
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator) {
		auto PlayerController = Cast<APongController>(*Iterator);
		if (PlayerController) {
			PlayerController->ClientSetMessage(Message, ExpireIn);
		}
	}
}

void APongGameModeBase::AddReadyPlayer(APongController* Player)
{
	if (!Player) {
		return;
	}

	auto Paddle = Cast<APaddle>(Player->GetPawn());
	if (Paddle && Instance) {
		Paddle->Speed = Instance->Settings.PaddleSpeed;
	}

	ReadyPlayers.AddUnique(Player);
	SendMessage(FString::Printf(TEXT("Player %s joined!"), *Player->PlayerState->GetPlayerName()));

	if (ReadyPlayers.Num() >= MinPlayers && !bIsStarted) {
		SendMessage("Minimum amount of players reached. Starting match in 3...", 3.f);
		StartGame();
	}
}

void APongGameModeBase::StartGame()
{
	auto Countdown = new uint8;
	*Countdown = 2;

	GetWorldTimerManager().SetTimer(StartGameCountDownTimer, [this, Countdown]() {
		if (*Countdown == 0) {
			SendMessage("Go!", 3.f);
			GetWorldTimerManager().ClearTimer(StartGameCountDownTimer);
			delete Countdown;
			bIsStarted = true;
			for (auto& Side : Players) {
				for (auto Player : Side.Value) {
					Player->SetMatchTime(MatchTime);
				}
			}
			SpawnBall();
			GetWorldTimerManager().SetTimer(MatchCountDownTimer, this, &APongGameModeBase::OnMatchEnd, Instance->Settings.MatchTime, false, -1.f);
		} else {
			SendMessage(FString::FromInt((*Countdown)--) + "...", 3.f);
		}
	}, 1.f, true, 1.f);
}

void APongGameModeBase::OnMatchEnd()
{
	for (auto& Side : Players) {
		for (auto Player : Side.Value) {
			Player->MatchEnd();
		}
	}

	Ball->Destroy();
}

void APongGameModeBase::SpawnBall()
{
	if (!BallClass) {
		UE_LOG(LogTemp, Warning, TEXT("No BallClass used in %s. You must set it before starting a match."), *GetClass()->GetName());
		return;
	}

	auto SpawnTransform = FTransform::Identity;
	SpawnTransform.SetLocation(FVector(0, 0, -10000));
	Ball = GetWorld()->SpawnActor<ABall>(BallClass, SpawnTransform);
	Ball->Speed = Instance ? Instance->Settings.BallSpeed * 100 : 1000.f;
	Ball->Respawn();
}

int32* APongGameModeBase::GetBotSideScore()
{
	for (auto& Side : Players) {
		if (Side.Value.Num() == 0) {
			return &Score[Side.Key];
		}
	}

	return nullptr;
}

void APongGameModeBase::ScorePoint(APongController* Player)
{
	if (!Player) {
		auto BotScore = GetBotSideScore();
		if (!BotScore) return;
		(*BotScore)++;
	} else {
		Score[Player->GetPlayerSide()]++;
	}

	for (auto& Side : Players) {
		for (auto P : Side.Value) {
			for (auto S : Score) P->SetMatchScore(S.Key, S.Value);
		}
	}
}