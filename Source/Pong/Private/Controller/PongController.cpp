// Fill out your copyright notice in the Description page of Project Settings.

#include "PongController.h"
#include "Camera/SideCamera.h"
#include "UserWidget/PongInterface.h"
#include "GameMode/PongGameModeBase.h"
#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>
#include <Net/UnrealNetwork.h>

APongController::APongController()
{
	bAutoManageActiveCameraTarget = false;
	SetReplicates(true);
}

void APongController::BeginPlay()
{
	Super::BeginPlay();

	if (InterfaceClass && IsLocalPlayerController()) {
		Interface = CreateWidget<UPongInterface>(this, InterfaceClass, "Interface");
		if (!Interface) {
			UE_LOG(LogTemp, Warning, TEXT("Couldn't create a widget from %s."), *InterfaceClass->GetName());
			return;
		}
		Interface->AddToViewport();
		Interface->SetVisibility(ESlateVisibility::Visible);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No InterfaceClass set in %s perhaps you should set it?"), *GetClass()->GetName());
	}

	Ready();
}

void APongController::Ready_Implementation()
{
	auto GameMode = GetWorld()->GetAuthGameMode<APongGameModeBase>();
	if (GameMode) {
		GameMode->AddReadyPlayer(this);
	}
}

bool APongController::Ready_Validate()
{
	return true;
}

void APongController::AssignSide(EPlayerSide Side, AActor* Camera)
{
	PlayerSide = Side;

	if (!Camera) {
		UE_LOG(LogTemp, Warning, TEXT("No camera found, perhaps you are using the wrong game mode? You should get additional logging from the game mode otherwise."));
		return;
	}

	PaddleCamera = Camera;

	// If we get a side after pawn possession (quite likely) just trigger the camera switch
	if (GetPawn() != nullptr) {
		OnPawnPossessed();
	}
}

void APongController::OnPawnPossessed()
{
	SetViewTargetWithBlend(PaddleCamera, 1.f);
}

AActor* APongController::GetPaddleCamera()
{
	return PaddleCamera;
}

void APongController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APongController, PaddleCamera);
	DOREPLIFETIME(APongController, PlayerSide);
}

void APongController::SetMessage(const FString& Message, float ExpireIn)
{
	if (!IsLocalPlayerController()) {
		ClientSetMessage(Message, ExpireIn);
		return;
	}

	if (Interface) {
		Interface->SetMessage(Message, ExpireIn);
	}
}

void APongController::ClientSetMessage_Implementation(const FString& Message, float ExpireIn)
{
	SetMessage(Message, ExpireIn);
}

void APongController::SetMatchTime(int32 MatchTime)
{
	if (!IsLocalPlayerController()) {
		ClientSetMatchTime(MatchTime);
		return;
	}

	if (Interface) {
		Interface->SetMatchTime(MatchTime);
	}
}

void APongController::ClientSetMatchTime_Implementation(int32 MatchTime)
{
	SetMatchTime(MatchTime);
}

void APongController::SetMatchScore(EPlayerSide Side, int32 Score)
{
	if (!IsLocalPlayerController()) {
		ClientSetMatchScore(Side, Score);
		return;
	}

	if (Interface) {
		Interface->SetMatchScore(Side, Score);
	}

	Scores.FindOrAdd(Side) = Score;
}

void APongController::ClientSetMatchScore_Implementation(EPlayerSide Side, int32 Score)
{
	SetMatchScore(Side, Score);
}

void APongController::MatchEnd()
{
	if (!IsLocalPlayerController()) {
		ClientMatchEnd();
		return;
	}

	if (Interface) {
		EMatchResult Result = EMatchResult::Draw;

		auto OtherSide = EPlayerSide::Side1 == PlayerSide ? EPlayerSide::Side2 : EPlayerSide::Side1;
		if (Scores.FindOrAdd(PlayerSide) > Scores.FindOrAdd(OtherSide)) {
			Result = EMatchResult::Won;
		} else if (Scores.FindOrAdd(PlayerSide) < Scores.FindOrAdd(OtherSide)) {
			Result = EMatchResult::Lost;
		}

		Interface->MatchEnd(Result);
	}
}

void APongController::ClientMatchEnd_Implementation()
{
	MatchEnd();
}