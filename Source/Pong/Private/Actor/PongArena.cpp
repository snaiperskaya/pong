// Fill out your copyright notice in the Description page of Project Settings.

#include "PongArena.h"
#include "Component/ArenaBorderComponent.h"
#include <Components/ShapeComponent.h>

// Sets default values
APongArena::APongArena()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Border1 = CreateDefaultSubobject<UArenaBorderComponent>(TEXT("Border1"));
	Border1->SetupAttachment(Root);
	Border2 = CreateDefaultSubobject<UArenaBorderComponent>(TEXT("Border2"));
	Border2->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void APongArena::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APongArena::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

