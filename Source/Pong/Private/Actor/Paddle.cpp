// Fill out your copyright notice in the Description page of Project Settings.

#include "Paddle.h"
#include "Controller/PongController.h"
#include "BlueprintLibrary/DebugLibrary.h"
#include <Components/StaticMeshComponent.h>
#include <Engine/World.h>
#include <Engine/StaticMesh.h>
#include <Net/UnrealNetwork.h>
#include <Kismet/KismetMathLibrary.h>
#include <Kismet/KismetSystemLibrary.h>

// Sets default values
APaddle::APaddle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComp = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = RootComp;

	PaddleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PaddleMesh"));
	PaddleMesh->SetupAttachment(RootComponent);
	PaddleMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);

	SetReplicates(true);
	// Looks a bit laggy with 100
	NetUpdateFrequency = 150;
	Speed = 10.f;
}

void APaddle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("MoveRight", this, &APaddle::MoveRight);
}

void APaddle::BeginPlay()
{
	Super::BeginPlay();
	Size = GetSize();

	if (IsLocallyControlled() && PlayerPaddleMaterial && PaddleMesh->GetStaticMesh()) {
		PaddleMesh->SetMaterial(0, PlayerPaddleMaterial);
	}
}

// Called every frame
void APaddle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/* Just debugging some math 
	auto RightBorder = GetActorLocation() + GetActorRightVector() * ((FMath::Max3(Size.X, Size.Y, Size.Z) / 2) + 10);
	auto LeftBorder = GetActorLocation() + GetActorRightVector() * ((FMath::Max3(Size.X, Size.Y, Size.Z) / 2) + 10) * -1.f;
	UKismetSystemLibrary::DrawDebugSphere(
		GetWorld(),
		RightBorder,
		20,
		64,
		FColor(255, 0, 0)
	);
	UKismetSystemLibrary::DrawDebugSphere(
		GetWorld(),
		LeftBorder,
		20,
		64,
		FColor(255, 0, 0)
	);
	*/
}

void APaddle::Restart()
{
	Super::Restart();
	auto PlayerController = Cast<APongController>(GetController());
	if (PlayerController) {
		PlayerController->OnPawnPossessed();
	}
}

void APaddle::MoveRight(float Value)
{
	if (!CanMoveInDirection(Value)) {
		return;
	}

	SetActorLocation(GetActorLocation() + GetActorRightVector() * Value * Speed);

	if (Role < ROLE_Authority) {
		ServerMoveRight(Value);
	}
}

void APaddle::ServerMoveRight_Implementation(float Value)
{
	MoveRight(Value);
}

bool APaddle::ServerMoveRight_Validate(float Value)
{
	return true;
}

bool APaddle::CanMoveInDirection(float Value)
{
	if (Value == 0.f) return false;
	// If Value is positive then we are going right
	float Direction = (Value > 0 ? 1.f : -1.f);
	// Let's assume the largest side is always the horizontal one
	float Offset = ((Size.GetMax() / 2) + 10);

	FVector DirectionVector = GetActorRightVector() * Direction;
	FVector Start = GetActorLocation() + DirectionVector * Offset;
	FVector End = GetActorLocation() + DirectionVector * 100000;
	FHitResult Result;
	if (GetWorld()->LineTraceSingleByChannel(Result, Start, End, ECollisionChannel::ECC_Visibility)) {
		// Is the border far enough from the paddle?
		return Result.Distance > 0;
	}

	return false;
}

FVector APaddle::GetSize()
{
	if (!PaddleMesh->GetStaticMesh()) {
		return FVector::ZeroVector;
	}

	return PaddleMesh->GetStaticMesh()->GetBounds().GetBox().GetSize();
}

void APaddle::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APaddle, Size);
	DOREPLIFETIME(APaddle, Speed);
}

float APaddle::GetOnHitDirectionModifier(FVector HitWorldLocation)
{
	auto LocalHitLocation = HitWorldLocation - GetActorLocation();
	auto Ratio = LocalHitLocation / Size;

	return Ratio.Y;
}

bool APaddle::IsBorder(FVector HitWorldLocation)
{
	auto LocalHitLocation = HitWorldLocation - GetActorLocation() + Size / 2; 
	return
		((int32)LocalHitLocation.Y <= 0 || (int32)LocalHitLocation.Y >= Size.Y) &&
		((int32)LocalHitLocation.X != 0 && (int32)LocalHitLocation.X != Size.X);
}
