﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Ball.h"
#include "Actor/PongArena.h"
#include "Component/ArenaBorderComponent.h"
#include "BlueprintLibrary/DebugLibrary.h"
#include "GameMode/PongGameModeBase.h"
#include "Controller/PongController.h"
#include <Net/UnrealNetwork.h>
#include <Components/SphereComponent.h>
#include <Kismet/KismetMathLibrary.h>
#include <Actor/Paddle.h>
#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>

// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	RootComponent = CollisionSphere;

	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BallMesh"));
	BallMesh->SetupAttachment(CollisionSphere);

	Speed = 2000;

	SetReplicates(true);
	SetReplicateMovement(true);
	bAlwaysRelevant = true;
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Role >= ROLE_Authority) {
		MoveForward(DeltaTime);
	}
}

void ABall::PlaySound_Implementation(USoundBase* Sound)
{
	if (!Sound) {
		return;
	}

	UGameplayStatics::PlaySound2D(this, Sound);
}

void ABall::MoveForward(float DeltaTime)
{
	if (Role < ROLE_Authority) return;

	static FHitResult SweepHit;
	SetActorLocation(GetActorLocation() + GetActorForwardVector() * Speed * DeltaTime, true, &SweepHit);

	if (SweepHit.bBlockingHit) {
		if (SweepHit.Component.Get() && SweepHit.Component->IsA<UArenaBorderComponent>() && Role >= ROLE_Authority) {
			PlaySound(ScorePointSound);
			//We just try to understand which paddle gets the point given how far it is from the ball.
			TArray<AActor*> Paddles;
			UGameplayStatics::GetAllActorsOfClass(this, APaddle::StaticClass(), Paddles);
			AActor* FarestPaddle = nullptr;
			float Distance = 0.f;

			for (auto Paddle : Paddles) {
				if (FarestPaddle == nullptr) {
					FarestPaddle = Paddle;
					Distance = (FarestPaddle->GetActorLocation() - GetActorLocation()).Size();
					continue;
				}

				float PaddleDistance = (Paddle->GetActorLocation() - GetActorLocation()).Size();
				if (PaddleDistance > Distance) {
					FarestPaddle = Paddle;
					Distance = PaddleDistance;
				}
			}

			LastHitPaddle = Cast<APaddle>(FarestPaddle);

			auto GameMode = Cast<APongGameModeBase>(GetWorld()->GetAuthGameMode());
			if (!GameMode) return;
			if (LastHitPaddle) {
				GameMode->ScorePoint(Cast<APongController>(LastHitPaddle->GetController()));
			}

			Respawn();
			return;
		}

		/*
		This is as simple as inverting the X axis if we have hit a vertical object (the paddles, unless hitting them on the borders)
		or inverting the Y axis if we hit a wall
		*/
		auto NewDirection = GetActorForwardVector();
		auto Paddle = Cast<APaddle>(SweepHit.Actor.Get());
		auto Offset = FRotator::ZeroRotator;
		if (Paddle) {
			PlaySound(PaddleHitSound);
			LastHitPaddle = Paddle;
			if (Paddle->IsBorder(SweepHit.ImpactPoint)) {
				NewDirection.Y *= -1.f; // We still invert Y if we hit a paddle border, pretty much like hitting a wall
			} else {
				NewDirection.X *= -1.f;
			}
			Offset.Yaw += FMath::Clamp(-60.f * Paddle->GetOnHitDirectionModifier(SweepHit.ImpactPoint), -60.f, 60.f);
		} else {
			PlaySound(WallHitSound);
			NewDirection.Y *= -1.f;
		}

		auto NewRotation = UKismetMathLibrary::MakeRotFromX(NewDirection) + Offset;
		NewRotation.Yaw = FMath::Clamp(NewRotation.Yaw, 91.f, 269.f);
		SetActorRotation(UKismetMathLibrary::MakeRotFromX(NewDirection) + Offset);
	}
}

void ABall::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABall, Speed);
}

void ABall::Respawn()
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(this, APongArena::StaticClass(), Actors);

	if (Actors.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("No PongArena found in the scene. Cannot respawn ball."));
		return;
	}
	auto Rotation = FRotator(0.f, 180.f, 0.f) * FMath::FRand();
	Rotation.Yaw = FMath::Clamp(Rotation.Yaw, 45.f, 135.f) * (FMath::FRand() > 0.5 ? 1.f : -1.f);
	SetActorRotation(Rotation);

	SetActorLocation(Actors[0]->GetActorLocation() + FVector(0, 0, 110));

	LastHitPaddle = nullptr;
}