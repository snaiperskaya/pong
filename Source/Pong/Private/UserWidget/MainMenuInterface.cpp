// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuInterface.h"
#include "Instance/PongGameInstance.h"
#if WITH_EDITOR
#include <UnrealEd.h>
#endif

void UMainMenuInterface::StartSinglePlayer()
{
	if (SinglePlayerLevel == "") {
		UE_LOG(LogTemp, Warning, TEXT("No SinglePlayerLevel set in %s."), *GetClass()->GetName());
		return;
	}

	if (GetWorld()) {
		auto Instance = Cast<UPongGameInstance>(GetWorld()->GetGameInstance());
		if (Instance) {
			Instance->bIsSinglePlayer = true;
		}
		GetWorld()->GetFirstPlayerController()->ClientTravel(SinglePlayerLevel, ETravelType::TRAVEL_Absolute);
	}
}

void UMainMenuInterface::ConnectTo(FString Url)
{
	if (GetWorld() && GetWorld()->GetFirstPlayerController()) {
		GetWorld()->GetFirstPlayerController()->ClientTravel(Url, ETravelType::TRAVEL_Absolute);
	}
}

void UMainMenuInterface::StartListening()
{
	if (MultiPlayerLevel == "") {
		UE_LOG(LogTemp, Warning, TEXT("No MultiPlayerLevel set in %s."), *GetClass()->GetName());
		return;
	}

	if (GetWorld() && MultiPlayerLevel != "") {
		const FString Url = MultiPlayerLevel + "?Listen";
		GetWorld()->ServerTravel(Url);
	}
}

void UMainMenuInterface::Quit()
{
#if WITH_EDITOR
	GUnrealEd->RequestEndPlayMap();
	return;
#endif
	FGenericPlatformMisc::RequestExit(false);
}