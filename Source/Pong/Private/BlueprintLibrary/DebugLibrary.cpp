// Fill out your copyright notice in the Description page of Project Settings.

#include "DebugLibrary.h"
#include <Kismet/KismetSystemLibrary.h>
#include <Engine.h>

void UDebugLibrary::DrawDebugSphere(UObject* WorldContextObject, FVector Where, float Radius, float Duration, FLinearColor Color, int32 Segments, float Thickness)
{
	UKismetSystemLibrary::DrawDebugSphere(WorldContextObject, Where, Radius, Segments, Color, Duration, Thickness);
}

void UDebugLibrary::DrawVisibleLineTrace(UObject* WorldContextObject, FVector From, FVector To)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	static FName TagName = TEXT("DebugLineTrace");
	World->DebugDrawTraceTag = TagName;

	FHitResult Result;
	FCollisionQueryParams Params;
	Params.TraceTag = TagName;
	World->LineTraceSingleByChannel(Result, From, To, ECollisionChannel::ECC_Visibility, Params);
}